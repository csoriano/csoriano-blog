---
title: "Desktop Icons New Maintainer & Release"
linktitle: Desktop Icons New Maintainer & Release
date: 2019-05-03
tags: ["gnome", "desktop icons"]
draft: false
---

Desktop icons just had a new release! Plenty of good things went in:

Sergio fixed the "Open in Terminal" menu item not opening in the desktop folder,
and also made the files appear from top to bottom, left to right, that more users were used to.
Also, we discovered that the settings panel wasn't getting translated, so Sergio fixed it too and now all
translations are in place.

We had another developer contributing too, Andrea Azzarone. Andrea fixed the rubberband
going over windows, quite a nice touch. Also added a way to group consecutive rubberbands with
<ctrl> and <shift>, which is definitively useful!

You can grab the new version in [tarball format](https://gitlab.gnome.org/World/ShellExtensions/desktop-icons/tags/19.01.3)
 or in [extensions.gnome.org](https://extensions.gnome.org/extension/1465/desktop-icons/). 

# A new Maintainer
And I have good news... Sergio has just being apointed as co-maintainer of the project!

Sergio has been working on the project constantly, with a high sense of responsability, for more than
9 months. In fact, I have been struggling to catch up with all the work he has been doing. 
Whenever I reviewed a merge request, another two were opened :-)

Sergio deserves the maintainer position, having more decision making responsabilities and to take over
 important tasks in the project.

Congrats Sergio!