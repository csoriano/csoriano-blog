---
title: "Why you can and should apply for the board"
linktitle: Why you can and should apply for the board
date: 2019-05-27
tags: ["gnome", "foundation", "board"]
draft: false
---

It's GNOME board elections time!

Community members can apply to become GNOME Foundation directors, and the process is quite easy, it’s just about sending an email to two mailing lists. We can improve on the number of participation though, and having a good amount of applicants is important for having a healthy foundation - the more applicants there are, the more likely that different views, skills and working areas are represented.

I believe one of the big factors of not having high participation in elections is the lack of knowledge of what the board does and how much of a commitment it is. Because of that, we question whether we are ready for taking on the position. While minutes published by the board are an excellent tool (and I really need to thank Phillip and Federico here), minutes usually don't tell the whole story.

In light of recent discussions on foundation list, I realized we also didn't explain yet how the work of the board has changed to a more strategic position, and what that entails.

So let me try to improve that, and explain why you should apply and why you definitely can apply.

# How does the work of the board look nowadays?

For the purpose of understanding what kind of work we do, let me split them in two types. Work that is about execution, and work that is about strategy.

Examples of execution based tasks are:

* Sending an email to a certain committee.
* Moving this wiki page to this other wiki page.
* Cleaning up minutes.
* Approving/declining small budget stuff such as stickers, release dinners, hackfests snacks, etc.
* Approving/declining big budget stuff such as GUADEC, GNOME Asia, services (CI, map titles, etc.), hardware, etc.
* Approving/declining legal things such as trademarks usage.

Examples of strategic tasks are:

* Defining a policy for travel sponsorship.
* Defining what GNOME software is.
* Defining committee responsibilities.
* Defining yearly goals.
* Defining trademarks and its usage.
* Defining budget spending policy for small events.
* Defining events bidding process.

You can already see that strategic tasks are usually done to support execution based tasks. However, execution based tasks are what actually delivers, and without them strategic tasks don’t have much use. Execution based tasks are about doing, strategic tasks are about thinking and creating.

The difference between those is quite significant, execution based tasks take around 1-2 weeks to complete, are quite independent of other people and have clear steps to do them. Strategic tasks take between 6 months and 2 years long to complete, they are dependent on other people, involving several discussions in various meetings and emails, they are quite more complex and usually involve different areas such as community, legal, market knowledge, etc. Most of the times you learn about them while you go, so while you gain significant experience on those areas, the ramp up to work on strategic tasks takes a while.

Historically, the board has been mostly execution based. The reason is that execution based tasks needs to be done no matter what. So what happened is that strategic tasks were delayed, or never done, in order to keeps things running. I would say the balance was around 90%-10% for execution based and strategic tasks respectively.

This last year that has changed significantly. The staff of the foundation is doing the heavy lifting on the execution based tasks, so now the job left for the board is mostly strategic. The balance has flipped to a 10%-90% split of execution based and strategic tasks. This is already implemented, there is almost nothing left to hand off to the staff nowadays.

The board tasks over the last ~6 months have been focused on those that are usually up to a board to do, most of them cannot be done by the staff due to legal or general charity guidance, such as setting the compensation for the ED or setting the goals for the foundation.

# Why can you apply?

Alright, so you think you have some motivation to be on the board. But maybe reading the candidacy emails or the section I wrote before makes you think that the tasks are too complex, or that the commitment to be on the board might be too big for you. Well, I want to debunk this here.

One particularity of the board is that everyone takes on things they want to take on. And no more than that. There isn't a “you didn't do anything for a month, you should do this other task” kind of situations. Some of us simply provide opinions here and there, either because we don't have time, or don't have the knowledge, or don't have the energy or interest on learning that knowledge.

And that is okay, specially if you don't have experience on dealing with the tasks mentioned in the previously, the rest of the board and the staff are aware of that, it's expected that you will help where you can. We are volunteers after all, and sometimes an opinion or a question is the most helpful thing we can do.

There are only a few things that are required: Being open to learn, a bit of proactivity, attending 1h weekly meetings, attending GUADEC, and lastly, some time and willingness to take on some work (1-2h per week, some weeks much more, some weeks none at all).

Some areas may not be interesting for you, but some others may. Once you are in the board, take the initiative to volunteer on those that you feel are of interest to you and what you will do will be already useful.

# Why should you apply?

You have some motivation to be on the board, but what's really in there for you? What do you get from it? For me, there are two
big benefits.

One is leading the community and having an impact on the direction of the project. If you are reading this, most probably you care about the project and the community behind it. This is usually the biggest motivation, and having a way to impact the project with your ideas is certainly a good feeling. Being on the board will give you the insight necessary to move forward with your high impact ideas, will open the doors to external entities that will help you drive those initiatives in a much easier way, and will give you certain skills that will make them most likely to succeed.

The second one is personal and career development. Personally speaking, I gained knowledge about handling changes that have impact for over (most probably) 100 people, negotiating deals with companies, handling legal issues and concerns, manage personal conflicts, hiring process and salaries, and in general, management and leadership.

In regards of soft skills, I can say that some opportunities on the board are around the level of Principal or Senior Principal Engineer, with the advantage that you take only on the things you feel comfortable with while allowing you to get out of the comfort zone with the support of the rest of the board and staff.

# Is there any downside?

I hope what I said so far has motivated you enough to apply, you can see there are quite a few advantages both personally and for the community for you to go ahead and apply.

So is there any downside? Honestly, I think there is only one. Fear of rejection.

I applied for first time to the board about 4 years ago. I got 5 first position votes out of 100. Well... honestly, this had an impact on me. I thought the community simply didn't want me, and that's all about it. I decided to not apply again.

Despite that, here I am, last year I got quite a few of those first position votes for myself.

The key is to not take it personally, this can happen for multiple reasons that are not specific to you as a person.

Maybe you proposed an initiative in your candidacy email and it was too early to make it happen, maybe simply you were a visionary. Or maybe members didn’t know you enough, or you lacked some experience in coordinating initiatives.

Good thing is, these can be improved quite easily. You can take the lead on small tasks that involve several contributors, you will get the experience of leading those initiatives and members will get to know you. Maybe simply go over 3.34 items and ask about their status and who can help move them forward? Or just improve Meson build along different modules? Maybe help with an existing initiative such as newcomers or Flatpak? How about coordinating an announcement with the engagement team? Or coordinating the release notes?

Owning tasks that go across different projects is a good way get elected the next time.

# Apply today

I think I speak for the whole board when I say that we are glad to answer any questions about what working for the board entails, so feel free to simply informally contact any of us on IRC or email and we will answer any questions as our individual availability permits.

Now is your turn, apply, apply and apply.

_Thanks Nuritzi and Rob for going through the document._