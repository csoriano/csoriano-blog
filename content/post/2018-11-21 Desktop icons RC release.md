---
title: "Desktop icons RC release"
linktitle: Desktop icons RC release
date: 2018-11-21
tags: ["gnome", "extension", "shell", "desktop icons"]
draft: false
---

So it's finally here, desktop icons release candidate for 1.0 is available now!

This means that all the features we wanted for 1.0 are implemented and we freeze
the implementation of new features. Now we will focus on polishing and removing
noisy or unnecessary stuff in the UI, fix weird behaviours and UX, fix bugs, etc.

### What's new?

Tons of stuff! We added multimonitor support, HiDPI support, renaming of files,
thumbnails support, DnD between files on the desktop, better rubberband selection...
Honestly, more than I thought we could do. You can read more about what's
new in the [release notes](https://gitlab.gnome.org/World/ShellExtensions/desktop-icons/tags/18.11rc).

![multimonitor](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/desktop_icons_RC/multimonitor.png#center)

Only feature that it's not implemented is DnD from the windows to the
desktop and the other way around. Maybe not a big deal since you can copy & paste
either by shortcuts or by the context menus. We will be glad to review the code
if someone wants to give it a chance and work on it.

### This release already overpasses what Nautilus offered
Nautilus desktop icons has never worked well in multimonitor setups. Files
get stuck hidden in some not reachable places due to the nature of Nautilus
that simply puts a big window as the desktop, instead of a window per monitor.

This extension finally implements a sane multimonitor handling, by having a flexible grid
per monitor. No more lost data...

Also, as you might know, the Nautilus desktop icons was Xorg only. This extension
is by nature Wayland ready.

### What's needed for 1.0?

Once the result feels polished enough and we are confident about the amount
of features we want to support, the default settings, etc. we will release 1.0.
My hope is that that will happen in the timeframe of one month from now.

Feel free to grab the new release in the [GNOME extensions website](https://extensions.gnome.org/extension/1465/desktop-icons/)
test it, report bugs, give feedback and of course, welcome to join the
development of this project if you want to contribute!

And finally, with this release I'm glad to announce and welcome Sergio Costas to join
as official developer to the project! He has done an excellent job implementing
tons of features we wanted for the 1.0 version.

Thanks to everyone who contributed in one way or another!

Enjoy

PD: Nautilus 3.30.4 is needed for the rename of files to work.