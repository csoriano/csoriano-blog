---
title: "GNOME Internships interns has been elected"
date: 2018-11-01
tags: ["gnome", "internships"]
draft: false
---

Hello all,

[GNOME Internships](https://wiki.gnome.org/Internships) projects and interns has been elected!

We had have strong applicants and quite a big amount of applications. If you are
not the elected don't be discouraged, it wasn't an easy choice.

This round we have to congratulate Ludovico de Nittis who will work in the
["USB Protection"](https://wiki.gnome.org/Internships/2018/Projects/USB-Protection)
project with his mentor Tobias Mueller. Congrats Ludovico!

The project goal is to increase the robustness against attacks via
malicious USB devices. Certainly a challenging goal! You can read extensive
information in the project wiki linked above, it's definitely quite interesting.

This round of internships is fund by the [privacy and security campaign](https://www.gnome.org/news/2013/07/gnome-raises-20000-to-enhance-security-and-privacy/)
we did at GNOME a few years ago, and we are happy we can put them into good use
to improve the security and privacy of GNOME users and donors.

Deeply thanks to all the donors! Withouth you this initiative wouldn't have been
possible. Also thanks to the mentors and applicants.

More on the admin side, it has been a little bumpy since its the first time
doing these internships. And well, this last week I have been a bit worried
by other stuff you already probably know about :)

If you have any question or feedback, don't hesitate to contact me on IRC
as csoriano or send an email to internships-admin@gnome.org