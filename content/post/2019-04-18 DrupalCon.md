---
title: "DrupalCon"
linktitle: DrupalCon
date: 2019-04-19
tags: ["drupal", "conference", "gnome", "gitlab", "debian"]
draft: false
---

Last week I attended [DrupalCon](https://events.drupal.org/seattle2019) in the
lovely city of Seattle invited by Tim, the executive director of Drupal.

Our plan was to have a panel discussion about the tooling we use in FOSS
organization such as GNOME, Debian, Drupal, etc. Specially since we recently transitioned
to GitLab. The panel discussion was between Tim himself, Alex Wirt from Debian,
Eliran Mesika and Tina Sturgis from GitLab and me. We were [5 out of 9 featured
speakers](https://events.drupal.org/seattle2019/builder)!

# Seattle
I took some days before the conference to adjust to the timezone, and to take
the oportunity and visit Seattle, my second time in US. Visited all the nice
public spaces like the [Space Needle](https://en.wikipedia.org/wiki/Space_Needle),
 the [Chihuly Garden and Glass](https://www.chihulygardenandglass.com), Science museum
and the [Museum of Pop Culture](https://en.wikipedia.org/wiki/Museum_of_Pop_Culture)
 where I had the oportunity to play guitar
for longer time than I want to admit.

![Chihuly](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/drupalcon2019/chihuly.jpg#center)
Chihuly Garden and Glass

![Space Needle](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/drupalcon2019/needle.jpg#center)
Me and the rainy wather seen from the Space Needle

Also let me tell you, the coffee in [Starbucks Reserve](https://www.starbucksreserve.com/en-us/locations/seattle) is much better than regular
Starbucks! However, nothing as good as [StoryVille Cafe](https://storyville.com/); they even gave me a mug
as a gift.

# DrupalCon

After getting used to the timezone and regained energies from visiting all
Seattle, it was time to go to DrupalCon. It was hosted in the Washington
State Convention Center, quite a big venue with a proportionaly attendance of 
3500 people.

![drupalcon](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/drupalcon2019/drupalcon.jpg#center)

Attendees were quite nice,
I got quite a few small chats just walking around looking for talks to attend. That was appreciated,
specially for someone like me that knew noone and didn't have much of an idea
about any of the products discussed there.

I attended a few talks about project management, community management, etc. and
realized they face similar issues that we have to deal with. For example, how to organize
a big community and drive new initiatives, how to empower and encourage people
to work on the things that matter most for the project, etc.

Also, since Drupal is moving to a more enterprise model, questions such as 
"How do we keep free software ideals strong in our community?" and 
"How do we keep community as strong as enterprise/company
contributions?" were raised too.

Only thing I would have hoped to have was a central communication channel were I could just jump in and
ask or propose to do things around. Their IRC was not very active, which was
surprising given the number of attendees. This is something that works for us at
GUADEC with the GNOME Telegram group.

# The breakfast and panel discussion
The day for the talk arrived, and after preparing it quite well (we
started its preparation almost a year ago!) we had a nice breakfast where all of us
finally met in person.

These informal chats have a lot of value to me, we discussed the challenges and
goals of Drupal for the future. Similar with GitLab, we discussed the future direction
of the company and project and its FOSS values and how open they are with their
strategy, specially as they get closer to their IPO.

The panel discussion went quite well, but the attendance was pretty low, most
likely because it was at 9:00 the day after the DrupalCon parties... hard to get
people in. Even then, we discussed with the Matthew Tift, a Drupal contributor in
 the "newcomers" field, for ways to improve community contribution. I also discussed with
Tina the marketing strategy in FOSS organizations that are community contributor
driven like GNOME, and ways to improve that for GNOME and GitLab.

![badge](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/drupalcon2019/badge.jpg#center)

The main point for Drupal was that historically they have been a collaboration island, in oposstion
of GNOME that we have been a collaboration
hub for most of the Linux desktop projects. Tim wants Drupal to improve this situation and
find ways to collaborate more with us, specially once I mentioned we were the
creators of Outreachy. Given that their main goal for this year is inclusivity
and diversity, GNOME being the creators of Outreachy gives us a great respect and trust on
driving such initiatives.

In general, my main message for the conference was about GNOME being not only a
desktop product, but also a place of inovation for FOSS projects and initiatives, and a place
where key people in the FOSS world have learn their skills while being driven by a community
rooted in FOSS values. GNOME goes beyond GNOME itself, and I hope this message got
 delivered!

# We should do more of this!
It was great to attend a conference for a product we don't have much in common,
we have many ways to collaborate that are not only about software, and organizations
like Drupal and GitLab are great partners in where we want to go and what we
want to do. I think, we at GNOME should do this more often, take these oportunities
to meet fellow FOSS projects and find more ways where we can intersect and collaborate.

Lastly, huge thanks to Tim and Drupal and Eliran, Tina and GitLab for sponsoring
my trip, and to Red Hat for giving me the time to go there. It was great to see
you all, I'm looking forward to more oportunities like these 🍻

![logos](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/drupalcon2019/logos.png#center)
