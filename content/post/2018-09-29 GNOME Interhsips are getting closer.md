---
title: "GNOME Internships are getting closer"
linktitle: GNOME Internships are getting closer 
date: 2018-09-29
tags: ["gnome", "internships", "privacy", "campaign"]
draft: false
---

### Almost there...

[GNOME Internships](https://wiki.gnome.org/Internships) are getting closer,
if you are a GNOME contributor with some experience this internship is a good
fit for you. Official application deadline is 1st of October, but we are open to receive more
aplications if the aplicant is a good fit.

How about applying? :) 

### What's this about?

GNOME internships is a program to use the donations we get from campaigns into projects
aimed to improve the topic of the campaign. This internship is not limited to students 
or a other level of expertise, quite the opossite, some experience and/or being part 
of GNOME already is a must.
Making sure these projects get finished is very important to us, and to achieve that the 
GNOME foundation is providing a more than nice stipend!

This round of internships is aimed to improve privacy when using GNOME. We have selected a [few projects](https://wiki.gnome.org/Internships/2018/Projects)
tha are key to provide an excellent privacy experience with GNOME software. We will use the funds we raised as part of the [privacy campaign](https://wiki.gnome.org/Foundation/PrivacyCampaign2013/) and we are excited to put them in good use for all of our donors.

### Requirements & more info

Ideal what-to-have are being a GNOME contributor and some work/other FOSS experience. If you
don't have work/other FOSS experience but are a regular GNOME contributor this is also fit
for you. Likewise, if you have work/other FOSS experience but don't have as many GNOME
contributions you can try out too after making some contributions to GNOME.

If you are not sure if you fullfill the requirements don't hesitate to ask me
or send an email to internships-admin@gnome.org. Above all, don't let the 
impostor syndrome to prevent you for asking or aplying!

Go to the [wiki page](https://wiki.gnome.org/Internships) to read more info and
follow the steps to apply.

Cheers 🐩
