---
title: "Moving from Wordpress"
subtitle: "A static website with Hugo"
date: 2018-09-11
tags: ["hugo", "gnome", "blog", "static website"]
draft: false
---

# Wordpress
![wordpress blog](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/moving_from_Wordpress/wordpress_blog.png#center)

I have been using Wordpress for long. Basically since I joined GNOME because it was required for GSoC. Wordpress works, it's okay. There are themes, it has a wysiwyg editor and you can embed images and videos quite easily. It kinda does the job...

## The bad
Now, one of the biggest problems are ads. Not that they exist, which is completely understandable, but rather that they are kinda crazy. I got reports that sometimes they were about guns in USA, or they lead to scam sites.

I also missed a way to link to my personal twitter/linkdin/gitlab. That's quite useful for people to check what other things I write about and how to get more info about what I do.

## The ugly
More on the technical side, one of the issues was that I coulnd't create a draft post and share it in private with other people so I could get some review. This was specially required for Nautilus anouncements. And in case I could, how could we collaborate in editing the post itself? That was simply not there.

Most importantly, I couldn't give personality to the blog. In the same way I want the software I use being neutral because for it's just a tool, my personal blog should be more repsentative on how I am and how I express myself. With Wordpress this was not possible. Hell, I couldn't even put some colors here and there, or take the bloat out from some of the widgets provided by the Wordpress themes.

# The enlightenment...
![didier's blog](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/moving_from_Wordpress/didier_blog.png#center)

Once upon a time, on a stormy day, I wrote the blog post about the removal and future plans of the desktop icons as a guest writter on [Didier's Roche blog](https://didrocks.fr/). And here came the enligthening, he was using some magic PR workflow in GitHub where I could just fix stuff, request to merge those changes, review happens, then gets accepted and published all automatically with CI.

Finally you could review, share drafts, and collaborate much easily than with Wordpress!

Not only that, but also his blog was much more personalized, closer to how he wanted to express himself.

## The decision
One thing I had have in the back of my mind for some time is that I need to improve my skills with non-GNOME stuff, specially web and cloud technologies. So what a better oportunity than trying to set up a static web generator for blogs (and other stuff) to mimic the features of Didier's blog?

And decided was it, got some free time this weekend and decided to learn this magic stuff.

## The technology
I decided to go with [Hugo](https://gohugo.io/), because back when I was experimenting with GitLab pages and a project page for Nautilus Hugo seemed to be the easiest and most convenient tech to create a static website that just works. Overall, it seems that Hugo is the most used static website generator for blogs.

I also decided that I would get a theme based in the well known and well maintained [bootstrap](https://getbootstrap.com/docs/3.3/css/). Most themes had some custom CSS, all delicately and manually crafted. But let's be honest, I don't want to maintain this, I wanted something simple I can go with.

So I chose the [minimal theme](https://themes.gohugo.io/minimal/) wich is based in bootstrap and then applied my own changes such as a second accent (the red in the titles), support for centered images, the Ubuntu font (which I use it everywhere I can), some navbar changes, full content rss support, lot of spacing adjustments, softer main text coloring, etc.

Also, I could finally put a decent comment section by using DisQus, which is added to Hugo with a single line. Eventually I would like to go with a free software solution such as [Talkyard](https://www.talkyard.io/), so far I didn't have luck to make it work.

The nicest thing of Hugo is that adding a new post is a matter of dropping a MarkDown file in the post folder. And that's it. That easy.

# The result
![The result](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/moving_from_Wordpress/hugo_blog.png#center)

So here's the result. I think it's quite an improvement versus what I had in Wordpress, although Antonio says that the page looks like Nautilus... I guess I cannot help myself. Let's see how it works in the future, specially since there is no wysiwyg editor. To be honest, using MarkDown is so easy that I don't see that as a problem so far.

I can even embed some code with highlighting for free:
```python
def do_measure(self, orientation: Gtk.Orientation,
               for_size: int) -> Tuple[int, int, int, int]:
    child = self.get_child()
    if not child:
        return -1, -1, -1, -1

    minimum, natural, _x, _x = child.measure(orientation, for_size)
    if self._max_width is not None and orientation == Gtk.Orientation.HORIZONTAL:
        natural = min(self._max_width, natural)

    return minimum, natural, -1, -1
```

And use Builder with Vim emulation to write this. That is already a big win!

If you want to take a look at the code or do something similar, feel free to take a look and use anything from [the code in GNOME's GitLab](https://gitlab.gnome.org/csoriano/csoriano-blog). I also added [an example post](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/post/example/) in order to see all formats for headings, bullet lists, images, code blocks, etc. Any feedback about the looks, functionality, content, etc. is welcome; finally I would be able to do something about it 😏.

*Hasta otra amigos* 🍹