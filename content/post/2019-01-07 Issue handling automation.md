---
title: "Issue handling automation for GNOME"
linktitle: Issue handling automation for GNOME 
date: 2019-01-07
tags: ["gnome", "issue", "automation", "bot"]
draft: false
---

I remember some time ago discussing with someone from GNOME how important is to make a good
issue report, the conclusion came along the lines of "I have 10 seconds to read
per issue, if the issue is not well done it's most likely I won't have time to
read it". It's true most of us are focused on doing actual code, after all it's
what most of us enjoy and/or what we are paid for. So bug handling always takes
a quite back place on our priorities.

On the other hand, the management of issues is neccessary for a healthy project,
specially if you are using it for planification, prioritization, feedback 
gatherer and interaction with other developers or teams. In general, a set
of open issues that is representative, updated and properly reported helps the project
progress and build a community around.

![bot](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/issue_automation/bot-cut.png#center)

# Handling issues automatically

There are a set of tasks that are quite repetitive, such as closing issues that
were left with the "need information" label. Or old feature requests. Or crashes
that were reported years ago with an old version of the software...

This is something I really wanted to get done with, so I've been working in the
past weeks to make a set up where repetitive tasks are automated by a GitLab CI
"bot", and this is what I'm gonna share with you today!

The tool that performs the task is used by GitLab CE itself for their own
issue triage automation and it's called
[gitlab-triage](https://gitlab.com/gitlab-org/gitlab-triage). This tool takes
a set of rules and process them with a CI job, then you can set to be run
periodically in schedules.

So let's take a look how to make a simple set up.

# Basic set up
First, you need to set up a CI job that will run the tool. For that, in your CI
file add:

```yaml
triage:
  image: ruby:2.4
  stage: triage
  script:
    - gem install gitlab-triage
    - gitlab-triage --token $TRIAGE_BOT_TOKEN --project-id $CI_PROJECT_PATH --host-url https://gitlab.gnome.org
  only:
    - schedules
```

And add a "triage" stage to the file stages. This will install a ruby docker
image, install the gitlab-triage program and then run it on [schedules](https://docs.gitlab.com/ee/api/pipeline_schedules.html).
Note the variable `$TRIAGE_BOT_TOKEN`, this is the API token of the account you
want to use for perform the actions. You can use your account, a new account, or
the bot I created for GNOME. Feel free to ask me the token for your use. Also, make
sure the project has a schedule that the triage can run on.

Now, what it should run tho? That's where the policies file enters the game.

# The policies
gitlab-triage will process a file called `.triage-policies.yml` on the root of your
project. This file defines with what rules and what the bot should do. You can
make conditions based on labels, dates, etc. Feel free to take a look at the 
[gitlab-triage documentation](https://gitlab.com/gitlab-org/gitlab-triage/blob/master/README.md),
it's quite extense and helpful.

For now, let's write our first rule. And we are going with the less controversial
one, closing issues that were marked as need information.

### Close issues that need information and weren't updated

When a bug needs information to be properly triaged, we mark issues with the
"need information" label. After some time, if nobody provides the information the bug
should be closed. At GNOME Bugzilla we were closing these bugs after 6 weeks, with a [stock answer](https://wiki.gnome.org/Bugsquad/TriageGuide/StockResponses)
.

How can we write this so it's done by the gitlab-triage bot? We just create
a new rule in the policy file, in this way:
```yaml
 resource_rules:
  issues:
    rules:
     - name: Close issues that need information and weren't updated 
        conditions:
          date:
            attribute: updated_at
            condition: older_than
            interval_type: weeks
            interval: 6
          state: opened
          labels:
            - 2. Needs Information 
        actions:
          status: close
          labels:
            - 15. Auto Updated
          comment: |
            Closing this issue as no further information or feedback has been provided.

            Please feel free to reopen this issue if you can provide the information or feedback.

            Thanks for your help!

            ---

            This is an automatic message. If you have suggestions to improve this automatic action feel free to add a comment on https://gitlab.gnome.org/GNOME/nautilus/issues/715
```
Quite simple. We set up the conditions that an issue must match in order
to be processed under "conditions", then we set the actions we want to do to
those issues in the "actions" section.

In this case, if an issue was last updated more than 6 weeks ago, it's in the opened state, and
has the label "2. Needs Information" it will close the issue, set the label
"15. Auto Updated" and comment the stock response. Note that we set a label to
know that it was autoupdated by a bot, so in case something goes wrong we can query
those issues and perform other actions on them.

You can see an example result [here](https://gitlab.gnome.org/GNOME/nautilus/issues/749#note_395818), it looks like:

![bot result](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/issue_automation/need_info_result.png#center)

And an example of a query for Nautilus auto updated issues [here](https://gitlab.gnome.org/GNOME/nautilus/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=15.%20Auto%20Updated), which looks like:

![auto updated result](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/issue_automation/auto_updated_results.png#center)

Nice, right?

### Close old feature proposals

Features proposals are probably the issues that has a higher ratio of being ignored. With the resources we usually have, it's unlikely we can add more maintainership "cost" to our projects, or that we already didn't plan carefully what features to add.

With this at hand, we want a rule that closes old feature proposals that hasn't been marked as part of the project planning. The rule looks like:
```yaml
- name: Close old feature proposals without planning labels or milestones
  conditions:
    date:
      attribute: created_at
      condition: older_than
      interval_type: months
      interval: 12
    labels:
      - 1. Feature
    forbidden_labels:
      - 2. Deliverable
      - 2. Stretch
      - 1. Epic
    milestone:
      - No Milestone
    state: opened
    upvotes:
      attribute: upvotes
      condition: less_than
      threshold: 10
  actions:
    labels:
      - 15. Auto Updated
    status: close
    comment: |
      Hi,

      First of all, thank you for raising an issue to help improving Nautilus. In order to maintain order in the issue tracker we are closing old, unscheduled feature proposals.

      Unfortunately, no Merge Request has been provided for this, and/or the project contributors are not planning this feature in the foreseeable future.

      This issue will be closed as it meets the following criteria:
      * Created more than 12 months ago
      * Labeled as ~"1. Feature"
      * Not associated with a milestone or with ~"2. Deliverable" or ~"2. Stretch" project planning labels.
      
      Thanks for your help!

      ---

      This is an automatic message. If you have suggestions to improve this automatic action feel free to add a comment on https://gitlab.gnome.org/GNOME/nautilus/issues/715
```
It's similar to the previous rule, we just added the condition to not proccess issues that has the project planing labels "2. Deliverable", "2. Stretch" or "1. Epic". The project planning labels comes from the [Project Planning for GNOME post](https://csorianognome.wordpress.com/2018/04/30/projects-release-planning-for-gnome/).

Note the voting threshold condition of 10 votes, this is just an internal way to make sure we don't close automatically a highly voted feature proposal, so we rather make a manual comment to avoid misscomunications.

You can see an example result [here](https://gitlab.gnome.org/GNOME/nautilus/issues/126#note_385647), it looks like:

![feature proposal result](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/issue_automation/feature_proposal_result.png#center)

### Bring attention to untriaged issues
This is all nice, but what about issues that wasn't even triagged in the first place? For those, we can make the bot create a summary for us. This helps greatly to bring to our attention in a regular basis those issues that need to be taken care of. 

We use the "summarize" action, the rule looks like:
```yaml
- name: Mark stale unlabelled issues for triage
  conditions:
    date:
      attribute: created_at
      condition: older_than
      interval_type: months
      interval: 2
    # We want to handle those that doesn't have these labels, including those with other labels.
    forbidden_labels:
      - 1. Bug
      - 1. Crash
      - 1. Epic
      - 1. Feature
    state: opened
  actions:
    labels:
      - 15. Untriaged
    summarize:
      title: Issues that need triaging
      item: |
        - {{web_url}} - {{title}} - {{labels}}
      summary: |
        The following issues were created two months ago and they are unlabeled:

        {{items}}

        /cc @Teams/BugSquad

```
This will create an issue with a list of issues that are lacking one of the labels listed in "forbidden_labels". Note that we could have use simply "No Label" value as a condition in a "Labels" section, however we wanted to bring to our attention those issues that have other labels but we didn't mark if they were a bug or a feature, because we rely on these labels for the previous rules. 

Also note that the created issue pings a group called "Teams/BugSquad", which doesn't exist yet. If this sounds useful for us, I would like to create this group so bug triagers can be part of that group and get pinged to handle these issues in a regular basis.

You can see an example result [here](https://gitlab.gnome.org/GNOME/nautilus/issues/826), it looks like:

![summary result](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/issue_automation/summary_result.png#center)
 
### Close old issues
This is a controversial one. Who hasn't receive a "Fedora has reached EOL" mass mails? :) I will leave the explanation why I think we should try this for another post, since here I want to focus on providing just the tooling and snippets for those who want to try it out.

For closing old bugs we just look at the updated date and close those that hasn't been touched in 18 months.
```yaml
- name: Close stale issues with no milestone or planning labels
  conditions:
    date:
      attribute: updated_at
      condition: older_than
      interval_type: months
      interval: 18
    milestone:
      - No Milestone
    forbidden_labels:
      - 2. Deliverable
      - 2. Stretch
      - 1. Epic
      # Features are handled in a different rule
      - 1. Feature
    state: opened
  actions:
    status: close
    labels:
      - 15. Auto Updated
    comment: |
      Hi,

      Thank you for raising an issue to help improve Nautilus. We're sorry this particular issue has gone unnoticed for quite some time.

      This issue will be closed, as it meets the following criteria:
      * No activity in the past 18 months (3 releases).
      * Unscheduled. Not associated with a milestone or with ~"2. Deliverable" or ~"2. Stretch" project planning labels.

      We'd like to ask you to help us keep our issue tracker organized  by determining whether this issue should be reopened.

      If this issue is reporting a bug, let us know if this issue is still present in a newer version and if you can reproduce it in the [nightly version](https://wiki.gnome.org/Apps/Nightly).

      Thanks for your help!

      ---

      This is an automatic message. If you have suggestions to improve this automatic action feel free to add a comment on https://gitlab.gnome.org/GNOME/nautilus/issues/715
```

# Let me know how it works for you
So far it has work quite well for Nautilus, in our first run it helped us to triage [around 50 issues](https://gitlab.gnome.org/GNOME/nautilus/issues/785), and the bot closes 15 issues that were either waiting long for information or were old features requests.

These rules can be tweaked, and in general these are an approach I think we should test. There are a few things that are a bit bold, specially closing issues automatically, so any feedback is appreciated.

I'm looking forward to hear how it works for you, and see if there are other modifications you do or some advice you would propose. 

Enjoy!