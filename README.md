Personal blog made with [Hugo](https://gohugo.io/), a theme based in 
[Bootstrap](https://getbootstrap.com/docs/3.3/css/) 
and modified [minimal hugo theme](https://themes.gohugo.io/minimal/).

https://csoriano.pages.gitlab.gnome.org/csoriano-blog